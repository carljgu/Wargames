package edu.ntnu.idatt2001.carljgu.units.specialized;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CavalryUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

/* Uses getAttackBonus and getResistBonus to simulate an attack */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CavalryTest {

    CavalryUnit cavalryOne;
    CavalryUnit cavalryTwo;
    CavalryUnit cavalryThree;

    @BeforeEach
    public void instantiateThreeCavalryUnits() {
        cavalryOne = new CavalryUnit("Cavalry One", 10);
        cavalryTwo = new CavalryUnit("Cavalry Two", 10);
        cavalryThree = new CavalryUnit("Cavalry Three", 10);
    }

    @Test
    public void cavalry_unit_attackBonus_differs_only_first_two_times() {
        int attackBonus1 = cavalryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonus2 = cavalryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonus3 = cavalryOne.getAttackBonus(Terrain.PLAINS);

        assertTrue(attackBonus1 > attackBonus2);
        assertEquals(attackBonus2, attackBonus3);
    }

    @Test
    public void cavalry_unit_attackBonus_is_more_on_plains() {
        int attackBonusPlains = cavalryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusHills = cavalryTwo.getAttackBonus(Terrain.HILLS);
        int attackBonusForest = cavalryThree.getAttackBonus(Terrain.FOREST);

        assertTrue(attackBonusPlains > attackBonusForest);
        assertEquals(attackBonusForest, attackBonusHills);
    }

    @Test
    public void cavalry_unit_resistBonus_is_less_in_forest() {
        int resistBonusPlains = cavalryOne.getResistBonus(Terrain.PLAINS);
        int resistBonusHills = cavalryTwo.getResistBonus(Terrain.HILLS);
        int resistBonusForest = cavalryThree.getResistBonus(Terrain.FOREST);

        assertTrue(resistBonusForest < resistBonusPlains);
        assertEquals(resistBonusPlains, resistBonusHills);
    }
}
