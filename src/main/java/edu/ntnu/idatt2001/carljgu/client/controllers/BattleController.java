package edu.ntnu.idatt2001.carljgu.client.controllers;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.Battle;
import edu.ntnu.idatt2001.carljgu.client.App;
import edu.ntnu.idatt2001.carljgu.client.BattleSimulationSingleton;
import edu.ntnu.idatt2001.carljgu.filehandling.FileExtensionException;
import edu.ntnu.idatt2001.carljgu.filehandling.ArmyFileHandler;
import edu.ntnu.idatt2001.carljgu.client.dialogs.DialogBox;
import edu.ntnu.idatt2001.carljgu.client.dialogs.DialogBoxBuilder;
import edu.ntnu.idatt2001.carljgu.battle.Army;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.UnitFactory;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.IntStream;

import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.fxml.FXML;
import javafx.stage.Window;

import static javafx.scene.control.Alert.AlertType.*;

/**
 * A controller class which
 * handles events in the fxml file battle-armies.fxml.
 * This includes requesting to import
 * armies and simulate the battle
 *
 * @version 2.0 14.05.2022
 * @author Carl Gützkow
 */
public class BattleController implements Initializable {

    @FXML private ChoiceBox<String> terrainChoiceBox = new ChoiceBox<>();
    @FXML private Button resetArmiesButton, resetAndBattleButton, editArmyOneButton, editArmyTwoButton;
    @FXML private Label armyOneImportPath, armyOneName, armyOneToString;
    @FXML private ListView<Unit> armyOneUnitsListView, armyTwoUnitsListView;
    @FXML private Label armyTwoImportPath, armyTwoName, armyTwoToString;

    private Label[] armyImportPathLabels, armyNameLabels, armySummaryLabels;
    private ListView<Unit>[] armyUnitListViews;
    private Button[] editArmyButtons;

    @FXML private ListView<String> attackList;
    @FXML private Label lastSimulation, score;

    /**
     * Imports an army for the first army.
     * Uses the system's file explorer to find the file.
     * 0 selects the first army.
     */
    @FXML public void importArmyOne() {
        importArmy(0);
    }

    /**
     * Imports an army for the second army.
     * Uses the system's file explorer to find the file.
     * 1 selects the second army.
     */
    @FXML public void importArmyTwo() {
        importArmy(1);
    }

    /**
     * Helper method to import an army and
     * display its information in the
     * given label elements.
     *
     * @param armyNumber int - the selected army to import.
     *             0 for army one and 1 for army two
     */
    private void importArmy(int armyNumber) {
        ArmyFileHandler fileHandler = new ArmyFileHandler();
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        try {
            String filePath = fileHandler.getFilePath("OPEN");
            if (filePath != null)
                simulation.setFilePathByNumber(armyNumber, filePath);
            else
                return;

            simulation.setOriginalArmyByNumber(armyNumber, fileHandler.readArmyFromFile(simulation.getFilePathByNumber(armyNumber)));
            displayArmy(armyNumber);

            simulation.setImportedLinesSkipped(fileHandler.getReadLinesSkipped());
            DialogBoxBuilder dialogBoxBuilder = new DialogBoxBuilder(INFORMATION)
                    .setTitle("Army imported")
                    .setMessage("Army was successfully imported.");

            if (simulation.getImportedLinesSkipped().size() > 0) {
                dialogBoxBuilder.setMessage(
                        dialogBoxBuilder.getMessage() + "\n" +
                        "In the process " + simulation.getImportedLinesSkipped().size() + " units were corrupted.\n" +
                        "Expand the \"Show details\" button to see all corrupted lines."
                )
                .setListMessage(simulation.getImportedLinesSkipped())
                .setImage("corrupt-file.png");
            }
            dialogBoxBuilder.build().showAndWait();


        } catch (FileExtensionException e) {
            new DialogBoxBuilder(ERROR)
                    .setTitle("File is not supported")
                    .setMessage(e.getMessage())
                    .build().showAndWait();
        } catch (IOException e) {
            new DialogBoxBuilder(ERROR)
                    .setHeader("File could not be loaded")
                    .build().showAndWait();
        } catch (IllegalArgumentException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage(e.getMessage())
                    .build().showAndWait();
        }
        simulation.resetScores();
        score.setText(simulation.getScoreByNumber(0) + " - " + simulation.getScoreByNumber(0));

        resetArmiesButton.setDisable(simulation.atLestOneOriginalArmyIsNull());
        resetAndBattleButton.setDisable(simulation.atLestOneOriginalArmyIsNull());
    }

    /**
     * Creates deep copies of the original armies and selects them as the current armies.
     * Creates a battle with those armies.
     *
     * @throws IllegalArgumentException thrown if an army or battle is invalid.
     * @throws NullPointerException thrown if an army is a null object.
     */
    public void deepCopyArmiesAndCreateBattle() throws IllegalArgumentException, NullPointerException {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();
        UnitFactory factory = new UnitFactory();

        int amountOfArmies = simulation.getArmiesAmount();
        for (int armyNumber = 0; armyNumber < amountOfArmies; armyNumber++) {
            List<Unit> deepCopiedUnits = factory.deepCopyBasicUnits(simulation.getOriginalArmyByNumber(armyNumber).getAllUnits());
            Army army = new Army(simulation.getCurrentArmyByNumber(armyNumber).getName(), deepCopiedUnits);
            simulation.setCurrentArmyByNumber(armyNumber, army);
        }

        simulation.setBattle(new Battle(
                simulation.getCurrentArmyByNumber(0),
                simulation.getCurrentArmyByNumber(1),
                Terrain.getTerrain(terrainChoiceBox.getValue())));
    }

    /**
     * Clear the simulation attack log and displays to the armies.
     *
     * @throws NullPointerException thrown if an army is a null object.
     */
    public void clearAttackListAndDisplayArmies() throws NullPointerException {
        attackList.getItems().clear();

        displayArmy(0);
        displayArmy(1);
    }

    /**
     * Run when clicking on reset armies.
     * Deep copies the armies and creates a new battle.
     * Also resets the labels.
     */
    @FXML public void resetArmies() {
        try {

            deepCopyArmiesAndCreateBattle();
            clearAttackListAndDisplayArmies();

        } catch (IllegalArgumentException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage(e.getMessage())
                    .build().showAndWait();
        } catch (NullPointerException e) {
            new DialogBoxBuilder(ERROR)
                    .setTitle("Army does not exist")
                    .setMessage("Armies have not been imported")
                    .build().showAndWait();
        }
    }

    /**
     * Run when clicking on the button reset and run.
     * Creates deep copies of the armies and runs the
     * simulation. Then it calls the methods in View
     * to display that information.
     */
    @FXML public void runSimulation() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        try {
            deepCopyArmiesAndCreateBattle();

            Army winningArmy = simulation.getBattle().simulate();


            if (winningArmy.equals(simulation.getCurrentArmyByNumber(0))) simulation.incrementScore(0);
            else simulation.incrementScore(1);
            score.setText(simulation.getScoreByNumber(0) + " - " + simulation.getScoreByNumber(1));
            lastSimulation.setText(winningArmy.getName());

            clearAttackListAndDisplayArmies();
            attackList.getItems().addAll(simulation.getBattle().getAttackLog());

        } catch (UnsupportedOperationException | IllegalArgumentException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage(e.getMessage())
                    .build().showAndWait();
        } catch (NullPointerException e) {
            new DialogBoxBuilder(ERROR)
                    .setTitle("Army does not exist")
                    .setMessage("Armies have not been imported")
                    .build().showAndWait();
        }
    }

    /**
     * Updates the information about an army
     * in the chosen elements.
     * Uses both armyNumber and army to be able to display armies that
     * have changed. For example after a battle.
     *
     * @param armyNumber int - 0 or 1 depending on which elements to use to display an army.
     * @throws NullPointerException thrown if the army is null
     */
    public void displayArmy(int armyNumber) throws NullPointerException {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();
        Army army = simulation.getCurrentArmyByNumber(armyNumber);
        if (army == null) throw new NullPointerException("Army is a a null object");

        armyNameLabels[armyNumber].setText(army.getName());

        armySummaryLabels[armyNumber].setText(army.strengthAndUnitRepresentation());
        armyImportPathLabels[armyNumber].setText(simulation.getFilePathByNumber(armyNumber));

        editArmyButtons[armyNumber].setDisable(simulation.getOriginalArmyByNumber(armyNumber) == null);

        armyUnitListViews[armyNumber].getItems().clear();
        armyUnitListViews[armyNumber].getItems().addAll(army.getAllUnits());
    }

    /**
     * Goes to the edit page of an imported army.
     * @param armyNumber int - the specified army's number to edit
     */
    public void editArmy(int armyNumber) {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();
        if (simulation.getOriginalArmyByNumber(armyNumber) == null) {
            new DialogBoxBuilder(ERROR)
                    .setMessage("Army is not imported.")
                    .build().showAndWait();
            return;
        }
        simulation.setCurrentArmyByNumber(armyNumber, simulation.getOriginalArmyByNumber(armyNumber));
        simulation.setCurrentArmyNumber(armyNumber);
        App.changeScene("edit-army.fxml");
    }

    /**
     * Edits the left army. Army one
     */
    @FXML public void editArmyOne() {
        editArmy(0);
    }

    /**
     * Edits the right army. Army two
     */
    @FXML public void editArmyTwo() {
        editArmy(1);
    }

    /**
     * Creates a dialog box with custom buttons
     * where the user can change which side to
     * create an army to. From there it changes scene
     * to edit a new army.
     */
    public void createArmy() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        DialogBox dialog = new DialogBoxBuilder(INFORMATION)
                .setMessage("Which side do you want to create an army for?")
                .setTitle("Create army").setHeader("Going to create army screen.")
                .build();
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(e -> dialog.close());

        ObservableList<ButtonType> buttons = dialog.getButtonTypes();
        buttons.clear();
        buttons.add(new ButtonType("Left army"));
        buttons.add(new ButtonType("Right army"));

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isEmpty())
            return;
        int armyNumber;
        if (result.get().getText().equals("Left army"))
            armyNumber = 0;
        else
            armyNumber = 1;
        simulation.setCurrentArmyNumber(armyNumber);
        simulation.setCurrentArmyByNumber(armyNumber, new Army("New army"));
        App.changeScene("edit-army.fxml");
    }

    /**
     * Run when the fxml file is first loaded.
     * Fills in the tables of information for the armies
     *
     * @param url URL - a Uniform Resource Loader
     * @param resourceBundle ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        armyNameLabels = new Label[] {armyOneName, armyTwoName};
        armyImportPathLabels = new Label[] {armyOneImportPath, armyTwoImportPath};
        armySummaryLabels = new Label[] {armyOneToString, armyTwoToString};
        armyUnitListViews = new ListView[] {armyOneUnitsListView, armyTwoUnitsListView};
        editArmyButtons = new Button[] {editArmyOneButton, editArmyTwoButton};

        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();
        IntStream.range(0, simulation.getArmiesAmount()).forEach(armyNumber -> {
            if (simulation.getOriginalArmyByNumber(armyNumber) != null) displayArmy(armyNumber);
        });

        resetArmiesButton.setDisable(simulation.atLestOneOriginalArmyIsNull());
        resetAndBattleButton.setDisable(simulation.atLestOneOriginalArmyIsNull());

        Arrays.stream(Terrain.values()).forEach(s -> terrainChoiceBox.getItems().add(s.getReadableName()));
        terrainChoiceBox.setValue(Terrain.PLAINS.getReadableName());
    }
}
