package edu.ntnu.idatt2001.carljgu.client;

import edu.ntnu.idatt2001.carljgu.battle.Army;
import edu.ntnu.idatt2001.carljgu.battle.Battle;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Singleton class to hold information for the controllers.
 *
 * @author Carl Gützkow
 * @version 1.1 20.05.2022
 */
public class BattleSimulationSingleton {

    private final static BattleSimulationSingleton battleSimulationSingleton =
            new BattleSimulationSingleton();
    private Battle battle;
    private final Army[] originalArmies;
    private final Army[] currentArmies;
    private final int[] scores;
    private final String[] filePaths;
    private List<String> importedLinesSkipped;
    private final int amountOfArmies;
    private int currentArmyNumber;

    /**
     * Private constructor to uphold
     * singleton design pattern.
     * initializes the arrays.
     */
    private BattleSimulationSingleton() {
        amountOfArmies = 2;
        originalArmies = new Army[amountOfArmies];
        currentArmies = new Army[amountOfArmies];
        scores = new int[amountOfArmies];
        Arrays.fill(scores, 0);
        filePaths = new String[amountOfArmies];
    }

    /**
     * Gets the battle simulation instance
     *
     * @return battleSimulationSingleton - BattleSimulationSingleton - the simulation object that holds the models' information
     */
    public static BattleSimulationSingleton getBattleSimulation() {
        return battleSimulationSingleton;
    }


    /**
     * Gets the current battle.
     *
     * @return battle - Battle - the current battle to simulate.
     */
    public Battle getBattle() {
        return battle;
    }

    /**
     * Sets the current battle.
     *
     * @param battle Battle - the current battle to simulate.
     */
    public void setBattle(Battle battle) {
        this.battle = battle;
    }

    /**
     * Gets the original army by an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @return Army - an army in the original army array.
     */
    public Army getOriginalArmyByNumber(int armyNumber) {
        return originalArmies[armyNumber];
    }

    /**
     * Sets original army at an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @param army Army - an army in the original army array.
     */
    public void setOriginalArmyByNumber(int armyNumber, Army army) {
        originalArmies[armyNumber] = army;
        currentArmies[armyNumber] = army;
    }

    /**
     * Checks if more than one army is null
     *
     * @return boolean - true if at least one army is a null object.
     */
    public boolean atLestOneOriginalArmyIsNull() {
        return Arrays.stream(originalArmies).anyMatch(Objects::isNull);
    }

    /**
     * Gets current army by an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @return Army - an army in the current army array.
     */
    public Army getCurrentArmyByNumber(int armyNumber) {
        return currentArmies[armyNumber];
    }

    /**
     * Sets current army at an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @param army Army - an army in the current army array.
     */
    public void setCurrentArmyByNumber(int armyNumber, Army army) {
        currentArmies[armyNumber] = army;
    }

    /**
     * Gets score by an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @return int - how many simulations an army has won.
     */
    public int getScoreByNumber(int armyNumber) {
        return scores[armyNumber];
    }

    /**
     * Sets score at an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @param score int - how many simulations an army has won.
     */
    public void setScoreByNumber(int armyNumber, int score) {
        scores[armyNumber] = score;
    }

    /**
     * Increment score at an army index number.
     *
     * @param armyNumber int - the index of the army.
     */
    public void incrementScore(int armyNumber) {
        scores[armyNumber]++;
    }

    /**
     * Fills the array with scores with zeroes
     */
    public void resetScores() {
        Arrays.fill(scores, 0);
    }

    /**
     * Gets file path by an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @return String - file path for an army at the army index number.
     */
    public String getFilePathByNumber(int armyNumber) {
        return filePaths[armyNumber];
    }

    /**
     * Sets file path at an army index number.
     *
     * @param armyNumber int - the index of the army.
     * @param filePath String - file path for an army at the army index number.
     */
    public void setFilePathByNumber(int armyNumber, String filePath) {
        filePaths[armyNumber] = filePath;
    }

    /**
     * Gets a list of all lines skipped when importing an army.
     *
     * @return importedLinesSkipped - List - which lines were skipped during importing and why.
     */
    public List<String> getImportedLinesSkipped() {
        return importedLinesSkipped;
    }

    /**
     * Sets which lines were skipped during import.
     *
     * @param importedLinesSkipped List - which lines were skipped during importing and why.
     */
    public void setImportedLinesSkipped(List<String> importedLinesSkipped) {
        this.importedLinesSkipped = importedLinesSkipped;
    }

    /**
     * Gets the amount of armies.
     *
     * @return int - amount of armies.
     */
    public int getArmiesAmount() {
        return amountOfArmies;
    }

    /**
     * Gets the current army number.
     *
     * @return int - the current army number.
     */
    public int getCurrentArmyNumber() {
        return currentArmyNumber;
    }

    /**
     * Sets current army index number.
     *
     * @param armyNumber int - army index number.
     */
    public void setCurrentArmyNumber(int armyNumber) {
        this.currentArmyNumber = armyNumber;
    }

    /**
     * Gets current army by current number.
     *
     * @return Army - the current army at the current army index number.
     */
    public Army getCurrentArmyByCurrentNumber() {
        return currentArmies[getCurrentArmyNumber()];
    }
}
