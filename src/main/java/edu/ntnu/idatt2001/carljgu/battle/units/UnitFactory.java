package edu.ntnu.idatt2001.carljgu.battle.units;

import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.RangedUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * A class to simply and elegantly create units.
 * Either one by one or many at once. Uses string
 * instead of enums to directly insert the
 * unit type from the file reader. In addition,
 * the exception is handled in the factory.
 *
 * @author Carl Gützkow
 * @version 1.3 13.04.2022
 */
public class UnitFactory {

    /**
     * Creates a unit between
     * InfantryUnit, RangedUnit,
     * CavalryUnit or CommanderUnit.
     * If the unit type could not be
     * found, it will return null instead.
     *
     * @param unitType UnitType - the type of unit to create.
     * @param name String - name of the created unit.
     * @param health int - amount of health the unit starts width
     * @return unit - Unit - an instance of specialized unit with name and health specified
     * @throws IllegalArgumentException thrown if the unit could not be created or
     * if the type of unit does not exist.
     */
    public Unit createUnit(UnitType unitType, String name, int health) throws IllegalArgumentException {
        if (unitType == UnitType.INFANTRY_UNIT) return new InfantryUnit(name, health);
        if (unitType == UnitType.RANGED_UNIT) return new RangedUnit(name, health);
        if (unitType == UnitType.CAVALRY_UNIT) return new CavalryUnit(name, health);
        if (unitType == UnitType.COMMANDER_UNIT) return new CommanderUnit(name, health);

        throw new IllegalArgumentException("Unit type does not exists");
    }

    /**
     * Returns a list of units.
     * Uses the createUnit method to create a
     * list of "amount" units with the same name and health.
     *
     * @param amount int - amount of units to create.
     * @param unitType UnitType - the type of unit to create.
     * @param name String - name of the created unit.
     * @param health int - amount of health the unit starts width
     * @return units - List - a list of units with the equal attributes.
     * @throws IllegalArgumentException thrown if amount is a negative number, if
     * unit could not be created or if unit type does not exist.
     */
    public List<Unit> createListOfUnits(int amount, UnitType unitType, String name, int health) throws IllegalArgumentException {
        if (amount < 0) throw new IllegalArgumentException("Amount can not be a negative number");

        List<Unit> units = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            units.add(createUnit(unitType, name, health));
        }
        return units;
    }

    /**
     * Deep copies units, but ignores overridden
     * attack and armor points.
     *
     * @param units List - a list of units.
     * @return new_units - List - a deep copied list of all units
     * @throws IllegalArgumentException thrown if any units could not be
     * created or if unit type does not exist.
     */
    public List<Unit> deepCopyBasicUnits(List<Unit> units) throws IllegalArgumentException {
        List<Unit> new_units = new ArrayList<>();
        for (Unit unit : units)
            new_units.add(createUnit(UnitType.getUnitType(unit.getClassName()), unit.getName(), unit.getHealth()));
        return new_units;
    }
}
