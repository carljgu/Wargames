package edu.ntnu.idatt2001.carljgu.units.specialized;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

/* Uses getAttackBonus and getResistBonus to simulate an attack */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class InfantryTest {

    InfantryUnit infantryOne;
    InfantryUnit infantryTwo;
    InfantryUnit infantryThree;

    @BeforeEach
    public void instantiateInfantryUnit() {
        infantryOne = new InfantryUnit("Infantry One", 10);
        infantryTwo= new InfantryUnit("Infantry Two", 10);
        infantryThree = new InfantryUnit("Infantry Three", 10);
    }

    @Test
    public void infantry_unit_attackBonus_is_constant() {
        int attackBonus1 = infantryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonus2 = infantryOne.getAttackBonus(Terrain.PLAINS);

        assertEquals(attackBonus1, attackBonus2);
    }

    @Test
    public void infantry_unit_attackBonus_is_more_in_forest() {
        int attackBonusPlains = infantryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusHills = infantryTwo.getAttackBonus(Terrain.HILLS);
        int attackBonusForest = infantryThree.getAttackBonus(Terrain.FOREST);

        assertTrue(attackBonusForest > attackBonusPlains);
        assertEquals(attackBonusPlains, attackBonusHills);
    }

    @Test
    public void infantry_unit_resistBonus_is_more_in_forest() {
        int resistBonusPlains = infantryOne.getResistBonus(Terrain.PLAINS);
        int resistBonusHills = infantryTwo.getResistBonus(Terrain.HILLS);
        int resistBonusForest = infantryThree.getResistBonus(Terrain.FOREST);

        assertTrue(resistBonusForest > resistBonusPlains);
        assertEquals(resistBonusPlains, resistBonusHills);
    }
}
