package edu.ntnu.idatt2001.carljgu.battle.units.specialized;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;

/**
 * Class CavalryUnit that inherits from Unit.
 * CavalryUnit objects implements methods from
 * the superclass.
 *
 * @author Carl Gützkow
 * @version 1.4 01.05.2022
 */
public class CavalryUnit extends Unit {

    /*
    Constants are defined here and given an integer
    for structure and simplification if they are to be changed.
    Constant meleeAttackBonus is used to calculate attack bonus.
    Constant resistBonus is used to return resist bonus.
    Constant plains_bonus is the bonus for attacking in the plains terrain.
    Constant forest_bonus is the resulting resist bonus when attacking in a forest
    Integer usedAttackBonus is the variable that is to be used
    for calculating attack bonus. In the method getAttackBonus
    this variable changes the first time it is run and therefore
    the variable can not be a constant.
     */
    private int usedAttackBonus = 6;
    private final int MELEE_ATTACK_BONUS = 2;
    private final int RESIST_BONUS = 1;
    private final int PLAINS_BONUS = 5;
    private final int FOREST_BONUS = 0;

    /**
     * Instantiates a new Cavalry unit.
     * Uses the constructor of its superclass Unit.
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     * @param attack int - damage done, excluding attack bonus, on an enemy unit.
     * @param armor  int - damage resisted, excluding resist bonus, from an enemy unit.
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new Cavalry unit with
     * a simplified constructor.
     * Attack is set to 20 and health is set to 12
     * Uses the constructor over since it instantiates other variables
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     */
    public CavalryUnit(String name, int health) {
        this(name, health, 20, 12);
    }

    /**
     * Overrides the abstract method getAttackBonus from Unit.
     * The first time it is run, the default usedAttackBonus
     * is used to simulate the first charge. After that the
     * attack bonus for melee is used instead throughout
     * the CavalryUnit's lifetime.
     * The bonus is also increased if the terrain is plains.
     *
     * @param terrain Terrain - the terrain where the attack occurs.
     * @return attackBonus - int - value of the attack bonus
     * used when calculating an enemy unit's health in an attack.
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = usedAttackBonus;
        usedAttackBonus = MELEE_ATTACK_BONUS;

        attackBonus += (terrain == Terrain.PLAINS) ? PLAINS_BONUS : 0;
        return attackBonus;
    }

    /**
     * Overrides the abstract method getResistBonus from Unit.
     * The resist bonus is increased if the terrain is forest.
     *
     * @param terrain Terrain - the terrain where the attack occurs.
     * @return resistBonus - int - value of the resist bonus
     * used when calculating this object's health in an attack.
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? FOREST_BONUS : RESIST_BONUS;
    }
}
