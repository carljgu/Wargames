package edu.ntnu.idatt2001.carljgu.battle.units.specialized;

/**
 * Class CommanderUnit that inherits from CavalryUnit.
 * CommanderUnit objects does not implement methods from
 * the superclass Unit as they are defined in CavalryUnit.
 *
 * @author Carl Gützkow
 * @version 1.4 01.05.2022
 */
public class CommanderUnit extends CavalryUnit {

    /**
     * Instantiates a new CommanderUnit unit.
     * Uses the constructor of its superclass CavalryUnit
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     * @param attack int - damage done, excluding attack bonus, on an enemy unit.
     * @param armor  int - damage resisted, excluding resist bonus, from an enemy unit.
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new CommanderUnit unit with
     * a simplified constructor.
     * Attack is set to 25 and health is set to 15
     * Uses the constructor of its superclass CavalryUnit since the constructor gives
     * no other value.
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }
}
