package edu.ntnu.idatt2001.carljgu.client.dialogs;

import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;

import java.util.List;

/**
 * A builder class which simplifies the process of
 * building a DialogBox object. Using a builder makes it easy
 * to create complex dialogs while still avoiding
 * the telescoping constructors problem.
 * It also makes it easier to add attributes in the future.
 *
 * @author Carl Gützkow
 * @version 1.2 01.05.2022
 */
public class DialogBoxBuilder {

    public static final String recurringDialogMessage =
            "If this a recurring and unsolvable event, please contact the creator of this program.";

    private AlertType alertType;
    private String title;
    private String header;
    private String message;
    private Image image;
    private List<String> listMessages;

    /**
     * Instantiates a new dialog box builder.
     * Sets the title and message to a default.
     * They are still optional to set.
     *
     * @param alertType AlertType - enum from inside the Alert class.
     *                  Either ERROR, INFORMATION, NONE, CONFIRMATION or WARNING
     */
    public DialogBoxBuilder(AlertType alertType) {
        this.alertType = alertType;
        this.title = alertType.name();
        this.message = recurringDialogMessage;
    }

    /**
     * Create the dialog box with the
     * current configurations from this
     * builder object.
     *
     * @return dialogBox - DialogBox - a dialog box with this builder's configurations.
     */
    public DialogBox build() {
        return new DialogBox(this);
    }

    /**
     * Set title to the dialog box builder configuration.
     * The title is displayed on the top of the window.
     *
     * @param title String - the title of the dialog box
     * @return dialogBoxBuilder - DialogBoxBuilder - returned to add further configurations.
     */
    public DialogBoxBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Add header to the dialog box builder configuration.
     * A header is displayed above the message
     *
     * @param header String - the header of the dialog box
     * @return dialogBoxBuilder - DialogBoxBuilder - returned to add further configurations.
     */
    public DialogBoxBuilder setHeader(String header) {
        this.header = header;
        return this;
    }

    /**
     * Set message to the dialog box builder configuration.
     *
     * @param message String - the message of the dialog box
     * @return dialogBoxBuilder - DialogBoxBuilder - returned to add further configurations.
     */
    public DialogBoxBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     * Add image to the dialog box builder configuration.
     * The image is displayed as both the window icon
     * and on the actual scene.
     *
     * @param imageName String - the name of the image to display.
     * @return dialogBoxBuilder - DialogBoxBuilder - returned to add further configurations.
     */
    public DialogBoxBuilder setImage(String imageName) {
        this.image = new Image(imageName, 50 ,50, false, false);
        return this;
    }

    /**
     * Adds a list message for the dialog box.
     *
     * @param listMessages - List - the dialog box' list of messages
     * @return dialogBoxBuilder - DialogBoxBuilder - returned to add further configurations.
     */
    public DialogBoxBuilder setListMessage(List<String> listMessages) {
        this.listMessages = listMessages;
        return this;
    }

    /**
     * Gets the alert type for the dialog box.
     *
     * @return alertType - AlertType - enum from inside the Alert class.
     *                                Either ERROR, INFORMATION, NONE, CONFIRMATION or WARNING
     */
    public AlertType getAlertType() {
        return alertType;
    }

    /**
     * Gets the title of the dialog box.
     *
     * @return title - String - the dialog box' title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the header for the dialog box.
     *
     * @return header - String - the dialog box' header
     */
    public String getHeader() {
        return header;
    }

    /**
     * Gets the message for the dialog box.
     *
     * @return message - String - the dialog box' message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the image for the dialog box.
     *
     * @return image - Image - the dialog box' icon and image
     */
    public Image getImage() {
        return image;
    }

    /**
     * Gets the list message for the dialog box.
     *
     * @return listMessages - List - the dialog box' list of messages
     */
    public List<String> getListMessages() {
        return listMessages;
    }
}
