package edu.ntnu.idatt2001.carljgu.client.controllers;

import edu.ntnu.idatt2001.carljgu.client.App;
import edu.ntnu.idatt2001.carljgu.client.BattleSimulationSingleton;
import edu.ntnu.idatt2001.carljgu.filehandling.ArmyFileHandler;
import edu.ntnu.idatt2001.carljgu.filehandling.FileExtensionException;
import edu.ntnu.idatt2001.carljgu.client.dialogs.DialogBoxBuilder;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.UnitFactory;
import edu.ntnu.idatt2001.carljgu.battle.units.UnitType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import static javafx.scene.control.Alert.AlertType.CONFIRMATION;
import static javafx.scene.control.Alert.AlertType.ERROR;

/**
 * A controller class which
 * handles events in the fxml file edit-army.fxml.
 * This includes requesting to import
 * armies and simulate the battle
 *
 * @version 1.9 14.05.2022
 * @author Carl Gützkow
 */
public class EditArmyController implements Initializable {

    @FXML public Label editArmyTitle, armyToString;
    @FXML public TextField editNameField;
    @FXML public Button editArmyNameButton, addUnitsButton, deleteUnitsButton;
    @FXML public ComboBox<String> unitsTypeComboBox = new ComboBox<>();
    @FXML public Spinner<Integer> amountOfUnitsSpinner, unitsHealthSpinner = new Spinner<>();
    @FXML public TextField unitsNameField;
    @FXML public ComboBox<Unit> allUnitsComboBox = new ComboBox<>();
    @FXML public ListView<Unit> armyUnitsListView;
    @FXML public ListView<String> changelogListView;
    @FXML public HBox hboxRoot;

    private ObservableList<String> observableChangelog;

    /**
     * Called for updating the army's name.
     */
    @FXML public void editArmyName() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        String newName = editNameField.getText();
        try {
            simulation.getCurrentArmyByCurrentNumber().setName(newName);
            updateComponentsWithInformation();
            displaySuccessfulPopupMessage("Name of army was successfully changed.");
            observableChangelog.add("✎ Changed name to " + newName);
        } catch (IllegalArgumentException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage(e.getMessage())
                    .build().showAndWait();
        }
    }

    /**
     * Called for adding new units.
     * Adds units with information about
     * amount of units, their name, their health,
     * and their unit type.
     */
    @FXML public void addUnits() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        String unitTypeClassName = unitsTypeComboBox.getValue();
        int amountOfUnits = amountOfUnitsSpinner.getValue();
        int unitsHealth = unitsHealthSpinner.getValue();
        String unitsName = unitsNameField.getText();

        UnitFactory factory = new UnitFactory();

        try {
            List<Unit> units = factory.createListOfUnits(amountOfUnits, UnitType.getUnitType(unitTypeClassName), unitsName, unitsHealth);
            simulation.getCurrentArmyByCurrentNumber().addUnits(units);
            updateComponentsWithInformation();
            displaySuccessfulPopupMessage("Successfully added to army.");
            observableChangelog.add("➕ Created " + amountOfUnits + " " + unitTypeClassName + ((amountOfUnits > 1) ? "s" : ""));
        } catch (IllegalArgumentException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage(e.getMessage())
                    .build().showAndWait();
        }
    }

    /**
     * Called for deleting a unit.
     */
    @FXML public void deleteUnit() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        Unit unitToDelete = allUnitsComboBox.getValue();
        try {
            boolean couldDelete = simulation.getCurrentArmyByCurrentNumber().remove(unitToDelete);
            if (couldDelete) {
                updateComponentsWithInformation();
                displaySuccessfulPopupMessage("Unit was successfully deleted.");
                observableChangelog.add("✖ Deleted " + unitToDelete.getClassName() + " " + unitToDelete.getName());
            }
        } catch (NullPointerException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage("Unit to delete is not selected.")
                    .build().showAndWait();
        }
    }

    /**
     * Cancel all changes and changes scene to the main page.
     * As the changes are stored in a deep copy, the scene
     * change disregards these changes.
     */
    @FXML public void cancelAllChanges() {
        Optional<ButtonType> result = new DialogBoxBuilder(CONFIRMATION)
                .setMessage("Are you sure you want to cancel editing this army? \n" +
                        "You will be returned to the battle page.")
                        .build().showAndWait();
        if (result.isPresent() && result.get().equals(ButtonType.OK))
            App.changeScene("battle-armies.fxml");
    }

    /**
     * Writes the changed army to a csv file.
     */
    @FXML public void writeToCSV() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        ArmyFileHandler fileHandler = new ArmyFileHandler();
        String filePath = fileHandler.getFilePath("SAVE");
        if (filePath == null)
            return;
        try {
            fileHandler.writeToFile(simulation.getCurrentArmyByCurrentNumber(), filePath);
            displaySuccessfulPopupMessage("Army was successfully written to file.");
        } catch (IOException e) {
            new DialogBoxBuilder(ERROR)
                    .setHeader("Army could not be written to file.").build().showAndWait();
        } catch (NullPointerException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage("No army has been chosen to edit.").build().showAndWait();
        } catch (FileExtensionException e) {
            new DialogBoxBuilder(ERROR)
                    .setMessage(e.getMessage()).build().showAndWait();
        }
    }

    /**
     * Update army and go back.
     * Sets the original army to the deep copied army.
     */
    @FXML public void updateArmyAndGoBack() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();
        Optional<ButtonType> result = new DialogBoxBuilder(CONFIRMATION)
                .setMessage("Are you sure you want to commit these changes? \n" +
                        "You will be returned to the battle page.")
                .build().showAndWait();
        if (result.isPresent() && result.get().equals(ButtonType.OK)) {
            simulation.setOriginalArmyByNumber(simulation.getCurrentArmyNumber(), simulation.getCurrentArmyByCurrentNumber());


            App.changeScene("battle-armies.fxml");
        }
    }

    /**
     * Display successful popup message.
     * Creates a new stage which is removed after a click.
     * Adds a blur effect behind the popup message that
     * is removed after it is hidden.
     *
     * @param successMessage the success message
     */
    public void displaySuccessfulPopupMessage(String successMessage) {
        hboxRoot.setEffect(new GaussianBlur(10));
        Popup popup = new Popup();
        Label label = new Label(successMessage);
        label.setStyle("-fx-text-fill: #058a05; -fx-background-color: white; -fx-label-padding: 10px; -fx-background-radius: 10px;" +
                "-fx-border-width: 4px; -fx-border-color: black; -fx-border-radius: 10px");
        popup.getContent().add(label);
        popup.show(App.getStage());
        popup.setOnAutoHide(s -> hboxRoot.setEffect(null));
        popup.setAutoHide(true);
    }

    /**
     * Update all fields and labels with updated information.
     */
    public void updateComponentsWithInformation() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        editArmyTitle.setText("Editing " + simulation.getCurrentArmyByCurrentNumber().getName());
        editNameField.setText("");
        unitsNameField.setText("");
        unitsTypeComboBox.getItems().clear();
        Arrays.stream(UnitType.values()).forEach(s -> unitsTypeComboBox.getItems().add(s.getClassName()));
        unitsTypeComboBox.setValue(UnitType.INFANTRY_UNIT.getClassName());

        armyToString.setText(simulation.getCurrentArmyByCurrentNumber().strengthAndUnitRepresentation());
    }

    /**
     * Adds event listeners to some fields to set
     * buttons to disabled if input is invalid.
     */
    public void addEventListenersToFieldsAndLists() {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        editNameField.textProperty().addListener(((observable, ignored, newValue) -> editArmyNameButton.setDisable(newValue.isBlank())));
        editArmyNameButton.setDisable(true);
        unitsNameField.textProperty().addListener(((observable, ignored, newValue) -> addUnitsButton.setDisable(newValue.isBlank())));
        addUnitsButton.setDisable(true);
        allUnitsComboBox.getSelectionModel().selectedItemProperty().addListener(((observable, ignored, newValue) -> deleteUnitsButton.setDisable(newValue == null)));
        deleteUnitsButton.setDisable(true);

        observableChangelog = FXCollections.observableArrayList();
        changelogListView.setItems(observableChangelog);
        armyUnitsListView.setItems((ObservableList<Unit>) simulation.getCurrentArmyByCurrentNumber().getAllUnits());
        allUnitsComboBox.setItems((ObservableList<Unit>) simulation.getCurrentArmyByCurrentNumber().getAllUnits());
    }

    /**
     * Run when the fxml file is first loaded.
     * Fills in the combo boxes and spinners
     *
     * @param url URL - a Uniform Resource Loader
     * @param resourceBundle ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        BattleSimulationSingleton simulation = BattleSimulationSingleton.getBattleSimulation();

        if (simulation.getCurrentArmyByCurrentNumber() == null) {
            return;
        }
        updateComponentsWithInformation();
        addEventListenersToFieldsAndLists();

        SpinnerValueFactory<Integer> amountValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 9999);
        SpinnerValueFactory<Integer> healthValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 9999);
        healthValueFactory.setValue(10);
        amountOfUnitsSpinner.setValueFactory(amountValueFactory);
        unitsHealthSpinner.setValueFactory(healthValueFactory);
    }
}
