package edu.ntnu.idatt2001.carljgu.client.dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.util.List;

/**
 * A dialog box that extends alert to use
 * a builder to build a dialog box.
 *
 * @author Carl Gützkow
 * @version 1.3 23.05.2022
 */
public class DialogBox extends Alert {

    /**
     * Constructor for a dialog box
     * that uses a builder to set
     * attributes. If the alert type is NONE,
     * the window will only close if there is at
     * least one button.
     * If the builder has no image defined, then
     * the default image is used. Otherwise, this
     * constructor will add image from resource folder.
     *
     * @param builder DialogBoxBuilder - a builder class to configure a dialog box
     */
    public DialogBox(DialogBoxBuilder builder) {
        super(builder.getAlertType());
        this.setTitle(builder.getTitle());
        this.setHeaderText(builder.getHeader());
        this.setContentText(builder.getMessage());

        if (builder.getAlertType() == AlertType.NONE) {
            this.getDialogPane().getButtonTypes().add(ButtonType.OK);
        }
        if (builder.getImage() != null) {
            setImage(builder.getImage());
        }
        if (builder.getListMessages() != null) {
            setListMessage(builder.getListMessages());
        }
    }

    /**
     * Sets the graphic of the alert to a given image.
     *
     * @param image Image - graphical image of the dialog box
     */
    public void setImage(Image image) {
        Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(image);
        this.setGraphic(new ImageView(image));
    }

    /**
     * Adds a list view node to the expandable content of the dialog.
     *
     * @param listMessage List - a list of strings.
     */
    public void setListMessage(List<String> listMessage) {
        ListView<String> list = new ListView<>();
        list.getItems().addAll(listMessage);
        getDialogPane().setExpandableContent(list);
    }
}
