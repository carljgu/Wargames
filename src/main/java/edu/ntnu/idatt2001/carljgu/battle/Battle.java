package edu.ntnu.idatt2001.carljgu.battle;

import edu.ntnu.idatt2001.carljgu.battle.units.Unit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;


/**
 * Class Battle that simulates a battle between two armies.
 *
 * @author Carl Gützkow
 * @version 1.4 01.05.2022
 */
public class Battle {

    /*
    Uses a list for storing the
    attacks because an unknown quantity
    of attacks is added. In addition,
    the order of attack is important.
    Lastly, there is no use for having
    a hashmap to store with an ID.
     */
    private final Army armyOne;
    private final Army armyTwo;
    private Army winner = null;
    private final Terrain terrain;
    private final List<String> attackLog;

    /**
     * Instantiates a new Battle.
     *
     * @param armyOne Army - the first army
     * @param armyTwo Army - the second army
     * @param terrain Terrain - on what terrain the battle is fought on.
     * @throws IllegalArgumentException thrown if one of the armies are empty, if armies are equal or if terrain is not defined
     * @throws NullPointerException     thrown if one of the armies are null object
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) throws IllegalArgumentException, NullPointerException {
        if (armyOne == null || armyTwo == null) throw new NullPointerException("Army is not defined");
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new IllegalArgumentException("Army can not be empty");
        if (Objects.equals(armyOne, armyTwo)) throw new IllegalArgumentException("Armies can not be the same");
        if (terrain == null) throw new IllegalArgumentException("Terrain is not defined");
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
        this.attackLog = new ArrayList<>();
    }

    /**
     * Gets the winner of the army
     * or a null object.
     *
     * @return winner - Army - winner of the simulated battle
     */
    public Army getWinner() {
        return winner;
    }

    /**
     * Returns the log of attacks
     * to easily hold the information
     * about each attack.
     *
     * @return attackLog - List - a list of all the attacks performed in the simulation.
     */
    public List<String> getAttackLog() {
        return attackLog;
    }

    /**
     * Gets the terrain where
     * the battle occurs.
     *
     * @return terrain - Terrain - on what terrain the battle is fought on.
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * Simulate a battle.
     * The attacking army is chosen at random.
     * One random unit from the attacking army attacks
     * a random unit from the defending army. Then, if
     * the defender survives, it attacks the first attacker.
     * If the defender has no health left, it is removed.
     * The simulations run until one army
     * has no units left.
     *
     * @return winner - Army - the army that won.
     */
    public Army simulate() {
        if (winner != null) throw new UnsupportedOperationException("Simulation has already been run");
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            Army initialAttackingArmy;
            Army initialDefendingArmy;

            if (new Random().nextBoolean()) {
                initialAttackingArmy = armyOne;
                initialDefendingArmy = armyTwo;
            } else {
                initialAttackingArmy = armyTwo;
                initialDefendingArmy = armyOne;
            }

            Unit initialAttacker = initialAttackingArmy.getRandom();
            Unit initialDefender = initialDefendingArmy.getRandom();

            int damageDealt = initialAttacker.attack(initialDefender, terrain);
            attackLog.add(attackStringRepresentation(damageDealt, initialAttacker, initialDefender, initialAttackingArmy, initialDefendingArmy));
            if (initialDefender.getHealth() <= 0) {
                initialDefendingArmy.remove(initialDefender);
            } else {
                damageDealt = initialDefender.attack(initialAttacker, terrain);
                attackLog.add(attackStringRepresentation(damageDealt, initialDefender, initialAttacker, initialDefendingArmy, initialAttackingArmy));
                if (initialAttacker.getHealth() <= 0)
                    initialAttackingArmy.remove(initialAttacker);

            }
        }

        winner = (armyOne.hasUnits()) ? armyOne : armyTwo;
        return winner;
    }

    /**
     * Information displayed is the
     * name of the attacking and defending army,
     * unit information like name and class
     * as well as how much damage was outputted
     * from the attack. In addition, the
     * defender's health after the attack is shown
     *
     * Example of an attack's representation:
     *      "InfantryUnit knight from Human army
     *      did 10 damage to
     *      RangedUnit crossbow orc from Orc army
     *      Current health of defender is now at 0"
     *
     * @param damageDealt int - how much health the defending unit lost
     * @param attacker Unit - the attacking unit
     * @param defender Unit - the defending unit
     * @param attackingArmy Army - the attacking army
     * @param defendingArmy Army - the defending army
     * @return String - string representation of an attack
     */
    public String attackStringRepresentation(int damageDealt, Unit attacker, Unit defender, Army attackingArmy, Army defendingArmy) {
        return (
                attacker.getClassName() + " " + attacker.getName() + " from " + attackingArmy.getName() + "\n" +
                        "did " + damageDealt + " damage to\n" +
                        defender.getClassName() + " " + defender.getName() + " from " + defendingArmy.getName() + ".\n" +
                        "Current health of the defender is now at " + defender.getHealth()
        );
    }

    /**
     * Overrides toString() method from Object.
     * Returns different strings depending on who
     * won the battle or if the battle is not simulated yet.
     *
     * Example:
     *             Winner:
     *             Army  Human army
     *             ------------------------
     *             Infantry:  10
     *             Ranged:  23
     *             Cavalry:  4
     *             Commander: 0
     *             ------------------------
     *             Loser:
     *             Army  Orc army
     *             ------------------------
     *             Infantry:  3
     *             Ranged:  10
     *             Cavalry:  4
     *             Commander: 3
     *             ------------------------
     *
     * @return String - string representation of the battle.
     */
    @Override
    public String toString() {
        if (winner == null) return ("Army one:\n" + armyOne + "\n Army two: \n" + armyTwo + "\n");
        if (winner.equals(armyOne)) return ("Winner:\n" + armyOne + "\n Loser: \n" + armyTwo + "\n");
        return "Winner:\n" + armyTwo + "\n Loser: \n" + armyOne + "\n";
    }
}
