# Wargames

A repository containing a simulation of two armies attacking each other. The project will be done in may.


## Running the program

To run the program you need jdk-17 and maven 3.8.5.

Install the program by zip, tar or `git clone git@gitlab.stud.idi.ntnu.no:carljgu/Wargames.git`

`cd Wargames`

`mvn clean javafx:run`


## Documentation

Gitlab Pages: [Wargames javadoc](https://carljgu.pages.stud.idi.ntnu.no/Wargames)


## Release 1.0

Added classes 
- Unit: An abstract class that specialized units can extend.
- Infantry: The most basic specialized unit.
- Ranged: Has less resistance bonus over time.
- Cavalry: Has less attack bonus the second time.
- Commander: Extends Cavalry, but a  little stronger attack and armor.
- Army: Contains several units.
- Battle: Simulates a battle between two armies. One unit from one army attacks an opponent.

## Release 2.0

Added classes
- FileHandler: A class to read and write an army to a file
- FileExtensionException: An exception thrown if an extension is wrong
- FileHandlerTest: Tests for testing file

Added methods
- get*Units: Methods using streams to get Infantry,
Ranged, Cavalry and Commander units.

## Realease 3.0

Added a GUI to the program, a terrain enum, a unit factory and dialogbox with respective builder.

Added classes
- DialogBox: A dialog box that extends alert to use a builder to build a dialog box.
- DialogBoxBuilder: A builder class which simplifies the creation process of a DialogBox object.
- BattleController: A controller class which handles events in the fxml file.
- App: Class that extends Application from javafx. Responsibility for creating the stage
- Main: The main class that is run when the application is called upon.
- UnitFactory: A class to simply and elegantly create units. Either one by one or many at once.
- UnitFactoryTest: A test class to test the unit factory

Added enum
- Terrain: An enum that represents the different terrains

## Release 4.0

Added classes
- EditArmyController: Controller class handling events in the edit-army fxml file.
- BattleSimulationSingleton: Class which holds model information for controllers.

Added enum
- UnitType: Represents the different unit types
