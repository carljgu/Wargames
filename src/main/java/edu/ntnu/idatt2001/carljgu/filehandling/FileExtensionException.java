package edu.ntnu.idatt2001.carljgu.filehandling;

/**
 * Thrown if an extension of a file is invalid
 * The exception thrown needs to have a message
 *
 * @author Carl Gützkow
 * @version 1.1 20.03.2022
 */
public class FileExtensionException extends IllegalArgumentException {

    /**
     * Constructs an exception with a detail message
     * @param message the detail message to display
     */
    public FileExtensionException(String message) {
        super(message);
    }
}
