package edu.ntnu.idatt2001.carljgu.units.specialized;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CommanderUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

/* commander inherits from cavalry so the same tests should
    work for commander. The first attack bonus is more than the rest */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CommanderTest {

    CommanderUnit commanderOne;
    CommanderUnit commanderTwo;
    CommanderUnit commanderThree;

    @BeforeEach
    public void instantiateCommanderUnit() {
        commanderOne = new CommanderUnit("Commander One", 10);
        commanderTwo = new CommanderUnit("Commander Two", 10);
        commanderThree = new CommanderUnit("Commander Three", 10);
    }

    @Test
    public void commander_unit_attackBonus_differs_only_first_two_times() {
        int attackBonus1 = commanderOne.getAttackBonus(Terrain.PLAINS);
        int attackBonus2 = commanderOne.getAttackBonus(Terrain.PLAINS);
        int attackBonus3 = commanderOne.getAttackBonus(Terrain.PLAINS);

        assertTrue(attackBonus1 > attackBonus2);
        assertEquals(attackBonus2, attackBonus3);
    }

    @Test
    public void commander_unit_attackBonus_is_more_on_plains() {
        int attackBonusPlains = commanderOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusHills = commanderTwo.getAttackBonus(Terrain.HILLS);
        int attackBonusForest = commanderThree.getAttackBonus(Terrain.FOREST);

        assertTrue(attackBonusPlains > attackBonusForest);
        assertEquals(attackBonusForest, attackBonusHills);
    }

    @Test
    public void commander_unit_resistBonus_is_less_in_forest() {
        int resistBonusPlains = commanderOne.getResistBonus(Terrain.PLAINS);
        int resistBonusHills = commanderTwo.getResistBonus(Terrain.HILLS);
        int resistBonusForest = commanderThree.getResistBonus(Terrain.FOREST);

        assertTrue(resistBonusForest < resistBonusPlains);
        assertEquals(resistBonusPlains, resistBonusHills);
    }
}
