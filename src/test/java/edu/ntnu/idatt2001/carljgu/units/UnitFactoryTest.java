package edu.ntnu.idatt2001.carljgu.units;

import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.UnitFactory;
import edu.ntnu.idatt2001.carljgu.battle.units.UnitType;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.RangedUnit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UnitFactoryTest {

    UnitFactory factory;
    UnitType validUnitType;
    String validName;
    int validHealth;

    @BeforeEach
    public void initializeUnitFactoryAndValidAttributes() {
        factory = new UnitFactory();
        validUnitType = UnitType.INFANTRY_UNIT;
        validName = "unit";
        validHealth = 10;
    }

    @Nested
    public class Exceptions_thrown {

        @Test
        public void unrecognized_unit_type_throws_exception_for_one_unit() {
            UnitType unvalidUnitType = UnitType.getUnitType("NotAType");

            assertThrows(IllegalArgumentException.class, () -> factory.createUnit(unvalidUnitType, validName, validHealth));
        }

        @Test
        public void unrecognized_unit_type_throws_exception_for_multiple_units() {
            UnitType unvalidUnitType = UnitType.getUnitType("NotAType");

            assertThrows(IllegalArgumentException.class, () -> factory.createListOfUnits(10, unvalidUnitType, validName, validHealth));
        }

        @Test
        public void amount_of_units_can_not_be_negative() {
            assertThrows(IllegalArgumentException.class, () -> factory.createListOfUnits(-1, validUnitType, validName, validHealth));
        }

    }

    @Nested
    public class Factory_deep_copies_units{

        @Test
        public void no_units_are_equal_to_previous_unit_list_after_deep_copy() {
            UnitFactory factory = new UnitFactory();
            List<Unit> units = new ArrayList<>();
            units.add(new InfantryUnit("Infantry", 10));
            units.add(new RangedUnit("Ranged", 10));

            List<Unit> deepCopiedUnits = factory.deepCopyBasicUnits(units);

            for (int i = 0; i < units.size(); i++) {
                assertNotEquals(units.get(i), deepCopiedUnits.get(i));
            }
        }

        @Test
        public void if_attack_and_armor_is_specified_for_unit_that_information_is_lost() {
            UnitFactory factory = new UnitFactory();
            List<Unit> units = new ArrayList<>();
            units.add(new InfantryUnit("Infantry", 10, 0, 0));

            List<Unit> deepCopiedUnits = factory.deepCopyBasicUnits(units);

            assertEquals(units.get(0).getHealth(), deepCopiedUnits.get(0).getHealth());
            assertNotEquals(units.get(0).getArmor(), deepCopiedUnits.get(0).getArmor());
        }

    }
}
