package edu.ntnu.idatt2001.carljgu.battle;

import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.RangedUnit;
import javafx.collections.FXCollections;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * public class Army.
 * An army object stores a list of units
 * meant to fight another army.
 *
 * @author Carl Gützkow
 * @version 1.4 18.05.2022
 */
public class Army {

    /* Chose list for units because several
     units are added and removed from the list
     and the units inside can be indexed. */
    private String name;
    private final List<Unit> units;

    /**
     * Instantiates a new Army.
     * Used if a list of units already exists.
     *
     * @param name String - the name of the army.
     * @param units List - the units of the army.
     * @throws IllegalArgumentException thrown if name is not valid.
     */
    public Army(String name, List<Unit> units) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be empty");
        this.name = name;
        this.units = FXCollections.observableList(units);
    }

    /**
     * Instantiates a new Army.
     * Second constructor if a list of units does not
     * exist from before.
     *
     * @param name String - the name of the army
     */
    public Army(String name) {
        this(name, new ArrayList<>());
    }

    /**
     * Gets the army's name.
     *
     * @return name - String - the name of the army.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the army's name
     *
     * @param name - String - the name of the army
     * @throws IllegalArgumentException thrown if name is not valid.
     */
    public void setName(String name) throws IllegalArgumentException{
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be empty.");
        this.name = name;
    }

    /**
     * Add unit if it does not exist from before.
     *
     * @param unit Unit - an instance of Unit.
     */
    public void addUnit(Unit unit) {
        if (!units.contains(unit)) units.add(unit);
    }

    /**
     * Add units.
     * Iterates over units and adds them if
     * they do not exist from before.
     *
     * @param units List - a list of units.
     */
    public void addUnits(List<Unit> units) {
        for (Unit unit : units) {
            addUnit(unit);
        }
    }

    /**
     * Remove a specified unit.
     *
     * @param unit Unit - an instance of Unit.
     * @return boolean - true if the unit was removed
     */
    public boolean remove(Unit unit) {
        return units.remove(unit);
    }

    /**
     * Checks if the list of units is empty
     *
     * @return boolean - true if the list is not empty.
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }

    /**
     * Gets all units.
     *
     * @return units - List - a list of units.
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Returns a list of infantry units
     * using stream and filter.
     *
     * @return List - a list of infantry units
     */
    public List<Unit> getInfantryUnits() {
        return units.stream().filter(n -> n instanceof InfantryUnit).toList();
    }

    /**
     * Returns a list of ranged units
     * using stream and filter.
     *
     * @return List - a list of ranged units
     */
    public List<Unit> getRangedUnits() {
        return units.stream().filter(n -> n instanceof RangedUnit).toList();
    }

    /**
     * Returns a list of cavalry units
     * using stream and filter.
     * Makes sure units are not commander units
     *
     * @return List - a list of cavalry units
     */
    public List<Unit> getCavalryUnits() {
        return units
            .stream()
            .filter(n -> n instanceof CavalryUnit && !(n instanceof CommanderUnit)).toList();
    }

    /**
     * Returns a list of commander units
     * using stream and filter.
     *
     * @return List - a list of commander units
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(n -> n instanceof CommanderUnit).toList();
    }

    /**
     * Gets a random unit from the list.
     * If there are no units it returns a null object.
     *
     * @return Unit - an instance of Unit.
     */
    public Unit getRandom() {
        if (hasUnits()) return units.get(new Random().nextInt(units.size()));
        return null;
    }

    /**
     * Calculates the strength of an army
     * by combining health, attack and armor
     * of every unit.
     *
     * @return int - the army's calculated strength
     */
    public int getCalculatedStrength() {
        return units.stream().mapToInt(s -> s.getHealth() + s.getAttack() + s.getArmor()).sum();
    }

    /**
     * Another method for return a representation
     * of the army. Includes the calculated strength
     * and how many of each unit type.
     * Example of a string representation:
     *             Strength: 302
     *             Infantry:  1
     *             Ranged:  5
     *             Cavalry:  1
     *             Commander: 0
     *
     * @return String - representation of the army.
     */
    public String strengthAndUnitRepresentation() {
        return  "Strength: " + getCalculatedStrength() + "\n" +
                "Infantry units: " + getInfantryUnits().size() + "\n" +
                "Ranged units: " + getRangedUnits().size() + "\n" +
                "Cavalry units: " + getCavalryUnits().size() + "\n" +
                "Commander units: " + getCommanderUnits().size() + "\n";
    }

    /**
     * Overrides the toString() method from Object.
     * iterates over all units and calls their toString() method.
     * Uses StringBuilder to avoid making new Strings.
     *
     * Example of a string representation:
     *             Army  Human army
     *             ------------------------
     *             Infantry:  10
     *             Ranged:  23
     *             Cavalry:  4
     *             Commander: 0
     *             ------------------------
     *
     * @return String - representation of the army.
     */
    @Override
    public String toString() {
        int infantryUnits = 0;
        int rangedUnits = 0;
        int cavalryUnits = 0;
        int commanderUnits = 0;

        for (Unit unit : units) {
            if (unit instanceof InfantryUnit) infantryUnits++;
            else if (
                unit instanceof RangedUnit
            ) rangedUnits++; else if (unit instanceof CavalryUnit) cavalryUnits++; else commanderUnits++;
        }

        return (
            "Army " + name + ":\n" +
            "------------------------\n" +
            "Infantry: " + infantryUnits + "\n" +
            "Ranged: " + rangedUnits + "\n" +
            "Cavalry: " + cavalryUnits +  "\n" +
            "Commander: " + commanderUnits + "\n" +
            "------------------------\n"
        );
    }

    /**
     * Checks if two armies are equal based on name and list of units.
     * @param o Object - object checking validity to.
     * @return boolean - true if name and list of units are equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Army)) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     * Hashes name and units for the equals method
     * @return int - hashcode to the army
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
