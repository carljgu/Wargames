package edu.ntnu.idatt2001.carljgu.battle.units.specialized;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;

/**
 * Class RangedUnit that inherits from Unit.
 * RangedUnit objects implements methods from
 * the superclass.
 *
 * @author Carl Gützkow
 * @version 1.4 01.05.2022
 */
public class RangedUnit extends Unit {

    /*
    Constants are defined here
    for structure and simplification if they are to be changed.
    Constant attackBonus is the attack bonus for the object's lifetime.
    Constant initialResistBonus is the first resistance bonus.
    Constant finalResistBonus is the default resistance bonus after a number
    of times resisted.
    Constant hill_bonus is a bonus for an attack on a hill
    Constant forest_penalty is a penalty for an attack in the forest.
    Integer timesResisted is used to calculate the resistance bonus.
    In the method getResistBonus this variable increases for each time the
    method is called. Therefore, it can not be a constant.
    */
    private final int ATTACK_BONUS = 3;
    private final int INITIAL_RESIST_BONUS = 6;
    private final int FINAL_RESIST_BONUS = 2;
    private final int HILL_BONUS = 3;
    private final int FOREST_PENALTY = 2;
    private int timesResisted = 0;

    /**
     * Instantiates a new Ranged unit.
     * Uses the constructor of its superclass Unit
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     * @param attack int - damage done, excluding attack bonus, on an enemy unit.
     * @param armor  int - damage resisted, excluding resist bonus, from an enemy unit.
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new Ranged unit with
     * a simplified constructor.
     * Attack is set to 15 and health is set to 8
     * Uses the constructor over since it instantiates other variables
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     */
    public RangedUnit(String name, int health) {
        this(name, health, 15, 8);
    }

    /**
     * Overrides the abstract method getAttackBonus from Unit.
     * The attack bonus is decreased if the terrain is forest
     * and increased if the terrain is hills.
     *
     * @param terrain Terrain - the terrain where the attack occurs.
     * @return attackBonus - int - value of the attack bonus
     * used when calculating an enemy unit's health in an attack.
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = ATTACK_BONUS;
        if (terrain == Terrain.HILLS) attackBonus += HILL_BONUS;
        if (terrain == Terrain.FOREST) attackBonus -= FOREST_PENALTY;
        return attackBonus;
    }

    /**
     * Overrides the abstract method getAttackBonus from Unit.
     * The first time it is run, times resisted is less than 2
     * and therefore returns the initial resist bonus.
     * The second time resisted is still less than 2, but
     * the initial resist bonus is subtracted by 2.
     * (initialResistBonus - 2 * timesResisted (1)).
     * The third time and all other times this method is,
     * the default finalResistBonus is used.
     *
     * @param terrain Terrain - the terrain where the attack occurs.
     * @return resistBonus - int - value of the resist bonus
     * used when calculating this object's health in an attack.
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int resistBonus = timesResisted >= 2 ? FINAL_RESIST_BONUS : INITIAL_RESIST_BONUS - 2 * timesResisted;
        timesResisted++;
        return resistBonus;
    }
}
