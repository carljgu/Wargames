package edu.ntnu.idatt2001.carljgu.units;

import edu.ntnu.idatt2001.carljgu.battle.units.UnitType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UnitTypeTest {

    @Test
    public void correct_enum_is_returned_from_string_else_null() {
        assertEquals(UnitType.INFANTRY_UNIT, UnitType.getUnitType("InfantryUnit"));
        assertEquals(UnitType.RANGED_UNIT, UnitType.getUnitType("RangedUnit"));
        assertEquals(UnitType.CAVALRY_UNIT, UnitType.getUnitType("CavalryUnit"));
        assertEquals(UnitType.COMMANDER_UNIT, UnitType.getUnitType("CommanderUnit"));
        assertNull(UnitType.getUnitType("Not A Unit"));
    }

    @Test
    public void correct_class_name_is_returned_from_enum() {
        assertEquals("InfantryUnit", UnitType.INFANTRY_UNIT.getClassName());
        assertEquals("RangedUnit", UnitType.RANGED_UNIT.getClassName());
        assertEquals("CavalryUnit", UnitType.CAVALRY_UNIT.getClassName());
        assertEquals("CommanderUnit", UnitType.COMMANDER_UNIT.getClassName());
    }

}
