package edu.ntnu.idatt2001.carljgu;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.carljgu.battle.Army;
import edu.ntnu.idatt2001.carljgu.filehandling.ArmyFileHandler;
import edu.ntnu.idatt2001.carljgu.filehandling.FileExtensionException;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.RangedUnit;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.TempDir;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ArmyFileHandlerTest {

    @TempDir
    public Path tempDir;

    Army army;
    ArmyFileHandler fileHandler;

    @BeforeEach
    public void initializeValidArmyWithSeveralUnitsOfAllClasses() {
        army = new Army("Army");

        ArrayList<Unit> units = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            units.add(new InfantryUnit("infantry", 10));
            units.add(new RangedUnit("ranged", 10));
            units.add(new CavalryUnit("cavalry", 10));
            units.add(new CommanderUnit("commander", 10));
        }

        army.addUnits(units);

        fileHandler = new ArmyFileHandler();
    }

    @Nested
    public class Write_army_to_file {

        @Test
        public void writing_to_file_does_not_cause_error_and_file_path_is_valid() {
            Path filePath = tempDir.resolve("valid-name.csv");

            assertDoesNotThrow(() -> fileHandler.writeToFile(army, filePath.toString()));
            assertTrue(Files.exists(filePath));
        }

        @Test
        public void exception_thrown_if_army_does_not_exist() {
            String filePath = tempDir.resolve("valid-name.csv").toString();

            assertThrows(NullPointerException.class, () -> fileHandler.writeToFile(null, filePath));
        }

        @Test
        public void file_extension_exception_thrown_if_file_is_not_csv() {
            String filePath = tempDir.resolve("unvalid-name.csv.cs").toString();

            assertThrows(FileExtensionException.class, () -> fileHandler.writeToFile(army, filePath));
        }

        @Test
        public void army_read_from_file_is_equal_to_army_written() {
            /* Does not use army's equals method as the unit list will be different */
            ArmyFileHandler fileHandler = new ArmyFileHandler();

            String filePath = tempDir.resolve("valid-name.csv").toString();
            Army readArmy = new Army("empty-army");

            try {
                fileHandler.writeToFile(army, filePath);
                readArmy = fileHandler.readArmyFromFile(filePath);
            } catch (IOException e) {
                fail();
            }

            assertEquals(readArmy.getAllUnits().size(), army.getAllUnits().size());
            assertEquals(readArmy.getName(), army.getName());
        }
    }

    @Nested
    public class Read_invalid_army_values_from_file {

        String filePath;

        @BeforeEach
        public void write_valid_army_to_file() {
            ArmyFileHandler fileHandler = new ArmyFileHandler();
            filePath = tempDir.resolve("valid-name.csv").toString();
            try {
                fileHandler.writeToFile(army, filePath);
            } catch (IOException e) {
                fail();
            }
        }

        @Test
        public void file_handler_reads_forty_one_units_from_file_when_several_extra_lines_are_not_valid_units_but_one_is() {
            ArmyFileHandler fileHandler = new ArmyFileHandler();

            int unitSize = army.getAllUnits().size();
            int rangedUnitSize = army.getRangedUnits().size();

            try (FileWriter fileWriter = new FileWriter(filePath, true)) {
                fileWriter.write("InfantryUnit,unit,2.0\n");
                fileWriter.write("InfantryUnit,unit,NaN\n");
                fileWriter.write("RangedUnit,infantry,10\n");
                fileWriter.write("CavalryUnit,infantry,-2\n");
                fileWriter.write("Unit,infantry,10\n");
            } catch (IOException e) {
                fail();
            }

            try {
                Army armyFromFile = fileHandler.readArmyFromFile(filePath);

                assertEquals(unitSize + 1, armyFromFile.getAllUnits().size());
                assertEquals(rangedUnitSize + 1, armyFromFile.getRangedUnits().size());
                assertEquals(4, fileHandler.getReadLinesSkipped().size());
            } catch (IOException e) {
                fail();
            }
        }

        @Test
        public void line_skipped_when_line_can_not_be_seperated() {
            ArmyFileHandler fileHandler = new ArmyFileHandler();

            try (FileWriter fileWriter = new FileWriter(filePath, true)) {
                fileWriter.write("InfantryUnit\n");
            } catch (IOException e) {
                fail();
            }

            try {
                fileHandler.readArmyFromFile(filePath);
            } catch (IOException e) {
                fail();
            }

            assertEquals(1, fileHandler.getReadLinesSkipped().size());
            assertEquals("Line \"InfantryUnit\" skipped because it did not contain enough information", fileHandler.getReadLinesSkipped().get(0));
        }
    }
}
