package edu.ntnu.idatt2001.carljgu.battle.units;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;

/**
 * The abstract class Unit.
 * A unit object should not be created because all units
 * that are used in the simulations are subclasses of Unit.
 * Since it is abstract a Unit object can not be created.
 *
 * @author Carl Gützkow
 * @version 1.5 15.05.2022
 */
public abstract class Unit {

    /*
    health, attack and armor are all integers as shown in the class diagram
    given. This should be okay as none of these numbers will be a divider or
    dividend. Health is the property that can be changed during the simulation.
     */
    private final String name;
    private int health;
    private final int attack;
    private final int armor;

    /**
     * Checks validity of name and health. Health can not be less than or equal to zero.
     * Attack and armor can not be negative, but can be equal to zero.
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     * @param attack int - damage done, excluding attack bonus, on an enemy unit.
     * @param armor  int - damage resisted, excluding resist bonus, from an enemy unit.
     * @throws IllegalArgumentException thrown if name is whitespace or contains comma or if health starts at zero or less.
     */
    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be empty");
        if (name.contains(",")) throw new IllegalArgumentException("Name can not contain comma");
        if (health <= 0) throw new IllegalArgumentException("Health can not start as less than or equal to 0");
        if (attack < 0) throw new IllegalArgumentException("Attack can not be negative");
        if (armor < 0) throw new IllegalArgumentException("Armor can not be negative");

        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Method for dealing damage to another unit.
     * It uses the enemy unit's attributes like health, resist bonus and armor
     * to calculate its remaining health.
     * If the total damage dealt is less than the armor and resistance bonus
     * for the defender, the unit's health is unaffected.
     *
     * @param unit Unit - enemy of any unit subclass.
     * @param terrain Terrain - the terrain where the attack is done.
     * @return totalDamage - int - the amount of damage the attacker inflicted
     */
    public int attack(Unit unit, Terrain terrain) {
        int totalDamage = this.getAttack() + this.getAttackBonus(terrain) - unit.getArmor() - unit.getResistBonus(terrain);
        if (totalDamage > 0) {
            unit.setHealth(Math.max(unit.getHealth() - totalDamage, 0));
            return totalDamage;
        }
        return 0;
    }

    /**
     * Gets name.
     *
     * @return name - String - a short name of the unit that can't be left with whitespace or empty.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the name of the class.
     *
     * @return class name - String - the name of the class
     */
    public String getClassName() {
        return getClass().getSimpleName();
    }

    /**
     * Gets health.
     *
     * @return health - int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Gets attack.
     *
     * @return attack - int - damage done, excluding attack bonus, on an enemy unit.
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets armor.
     *
     * @return armor - int - damage resisted, excluding resist bonus, from an enemy unit.
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Sets health.
     *
     * @param health - int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Overrides toString() method from Object.
     * Example:
     * InfantryUnit infantry with 15 health
     * has 10 attack and 15 armor.
     *
     * @return String - string representation of object
     */
    @Override
    public String toString() {
        return getClassName() + " " + name + " with " + health + " health"+ "\n" + "has " + attack + " attack and " + armor + " armor.";
    }

    /**
     * Gets attack bonus.
     * Abstract method that is to be defined in subclasses.
     *
     * @param terrain Terrain - the terrain of the attack
     * @return the attack bonus
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * Gets resist bonus.
     * Abstract method that is to be defined in subclasses.
     *
     * @param terrain Terrain - the terrain of the attack
     * @return the resist bonus
     */
    public abstract int getResistBonus(Terrain terrain);
}
