package edu.ntnu.idatt2001.carljgu.client;

import java.io.IOException;
import java.util.Arrays;

import edu.ntnu.idatt2001.carljgu.client.dialogs.DialogBoxBuilder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import static javafx.scene.control.Alert.AlertType.ERROR;

/**
 * Class that extends Application
 * from javafx.
 * Responsibility for creating the stage.
 *
 * @author Carl Gützkow
 * @version 1.3 14.05.2022
 */
public class App extends Application {

    private static Stage stage;

    /**
     * Overridden from Application.
     * Loads in the scene and configures the stage.
     *
     * @param startStage - Stage - the stage provided by javaFX
     */
    @Override
    public void start(Stage startStage) {
        stage = startStage;
        stage.setTitle("Wargames");
        stage.setMaximized(true);
        stage.setScene(new Scene(new AnchorPane()));
        stage.getScene().getStylesheets().add("stylesheet.css");
        stage.getIcons().add(new Image("wargames-logo.png"));

        if (changeScene("battle-armies.fxml"))
            stage.show();

    }

    /**
     * Method called from Main when the program is run
     * Calls on launch in Application
     *
     * @param args - String[] - commandline arguments
     */
    public static void run(String[] args) {
        launch();
    }

    /**
     * Gets the main stage for the program
     *
     * @return stage - Stage - the starting program stage
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * Changes the scene of the stage.
     *
     * @param sceneFileName String - file name with extension to the scene to change to.
     * @return boolean - true if the scene was changed. False otherwise.
     */
    public static boolean changeScene(String sceneFileName) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getClassLoader().getResource(sceneFileName));
            stage.getScene().setRoot(fxmlLoader.load());

            return true;

        } catch (IOException | IllegalStateException e) {
            new DialogBoxBuilder(ERROR)
                    .setHeader("Scene could not be loaded in.\n" +
                            "Expand the \"Show details\" button to see full stack trace.\n" +
                            "Error message: " + e.getMessage())
                    .setMessage(e.getMessage())
                    .setListMessage(Arrays.stream(e.getStackTrace()).map(String::valueOf).toList())
                    .build().showAndWait();
            return false;
        }
    }
}
