package edu.ntnu.idatt2001.carljgu.client;

/**
 * The main class that is run when the application
 * is called upon. Calls on the main method in App.
 * This class exists to have separate the main method
 * from javaFX when creating for example JLink.
 *
 * @author Carl Gützkow
 * @version 1.1 27.03.2022
 */
public class Main {

    /**
     * Method to first be launched when the program is run
     *
     * @param args - String[] - commandline arguments
     */
    public static void main(String[] args) {
        App.run(args);
    }
}
