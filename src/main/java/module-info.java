module edu.ntnu.idatt2001.carljgu.client {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.carljgu.client.controllers to javafx.fxml;
    exports edu.ntnu.idatt2001.carljgu.client ;
}
