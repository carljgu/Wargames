package edu.ntnu.idatt2001.carljgu.units;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class WarUnitTest {

    private Unit unit;

    @Nested
    public class Unit_constructor_throws_exception {

        @Test
        public void exception_for_name_thrown_if_name_only_contains_whitespace() {
            try {
                unit = new InfantryUnit("  ", 1);
                fail();
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Name can not be empty");
            }
        }

        @Test
        public void exception_for_health_thrown_if_health_is_less_than_or_equal_to_zero() {
            try {
                unit = new InfantryUnit("Valid name", -1);
                fail();
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Health can not start as less than or equal to 0");
            }
        }

        @Test
        public void exception_is_thrown_if_attack_is_negative() {
            try {
                unit = new InfantryUnit("Valid name", 1, -1, 1);
                fail();
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Attack can not be negative");
            }
        }

        @Test
        public void exception_is_thrown_if_armor_is_negative() {
            try {
                unit = new InfantryUnit("Valid name", 1, 1, -1);
                fail();
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Armor can not be negative");
            }
        }

        @Test
        public void exception_is_not_thrown_if_attack_or_armor_is_zero() {
            try {
                unit = new InfantryUnit("Valid name", 1, 0, 0);
                assertTrue(true);
            } catch (IllegalArgumentException exception) {
                fail();
            }
        }
    }

    @Nested
    public class Unit_attacking_a_defender {

        Unit attacker;
        Unit defender;

        public void instantiateAttackerAndDefender(int attackerArmor, int defenderArmor) {
            attacker =
                new Unit("Unit", 10, attackerArmor, 0) {
                    @Override
                    public int getAttackBonus(Terrain terrain) {
                        return 0;
                    }

                    @Override
                    public int getResistBonus(Terrain terrain) {
                        return 0;
                    }
                };

            defender =
                new Unit("Unit", 10, 0, defenderArmor) {
                    @Override
                    public int getAttackBonus(Terrain terrain) {
                        return 0;
                    }

                    @Override
                    public int getResistBonus(Terrain terrain) {
                        return 0;
                    }
                };
        }

        @Test
        public void unit_with_attack_ten_and_no_bonus_does_ten_damage_if_defender_has_no_resistBonus_or_armor() {
            instantiateAttackerAndDefender(10, 0);

            int startHealth = defender.getHealth();
            attacker.attack(defender, Terrain.PLAINS);
            int endHealth = defender.getHealth();

            assertEquals(10, startHealth - endHealth);
        }

        @Test
        public void health_is_unaffected_if_total_attack_is_less_than_total_resistance() {
            instantiateAttackerAndDefender(5, 10);

            int startHealth = defender.getHealth();
            attacker.attack(defender, Terrain.PLAINS);
            int endHealth = defender.getHealth();

            assertEquals(startHealth, endHealth);
        }
    }
}
