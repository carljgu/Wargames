package edu.ntnu.idatt2001.carljgu.battle;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * An enum that represents the different terrains
 * that the battles can occur on.
 *
 * @author Carl Gützkow
 * @version 1.2 15.05.2022
 */
public enum Terrain {
    PLAINS("Plains"),
    FOREST("Forest"),
    HILLS("Hills");

    private final String readableName;
    private static final Map<String, Terrain> terrains = Arrays.stream(Terrain.values())
            .collect(Collectors.toMap(Terrain::getReadableName, Function.identity()));

    /**
     * The enum constructor
     *
     * @param readableName String - the enum in PascalCase
     */
    Terrain(String readableName) {
        this.readableName = readableName;
    }

    /**
     * Gets the name from the enum
     *
     * @return readableName - String - enum in PascalCase
     */
    public String getReadableName() {
        return readableName;
    }

    /**
     * Gets a Terrain enum from a string.
     * If a terrain was not found, null is returned instead.
     *
     * @param readableName String - the enum in PascalCase
     * @return UnitType - the enum respective to the readable name
     */
    public static Terrain getTerrain(String readableName) {
        return terrains.getOrDefault(readableName, null);
    }
}
