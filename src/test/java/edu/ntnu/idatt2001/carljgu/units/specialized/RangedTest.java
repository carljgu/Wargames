package edu.ntnu.idatt2001.carljgu.units.specialized;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.RangedUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

/* Uses getAttackBonus and getResistBonus to simulate an attack */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class RangedTest {

    RangedUnit rangedOne;
    RangedUnit rangedTwo;
    RangedUnit rangedThree;

    @BeforeEach
    public void instantiateRangedUnit() {
        rangedOne = new RangedUnit("Ranged One", 10);
        rangedTwo = new RangedUnit("Ranged Two", 10);
        rangedThree = new RangedUnit("Ranged Three", 10);
    }

    @Test
    public void ranged_unit_resistBonus_subtracts_only_first_three_times() {
        int resistBonus1 = rangedOne.getResistBonus(Terrain.PLAINS);
        int resistBonus2 = rangedOne.getResistBonus(Terrain.PLAINS);
        int resistBonus3 = rangedOne.getResistBonus(Terrain.PLAINS);
        int resistBonus4 = rangedOne.getResistBonus(Terrain.PLAINS);

        assertTrue(resistBonus1 > resistBonus2);
        assertTrue(resistBonus2 > resistBonus3);
        assertEquals(resistBonus3, resistBonus4);
    }

    @Test
    public void infantry_unit_attackBonus_is_more_in_hills_and_less_in_forest() {
        int attackBonusPlains = rangedOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusHills = rangedTwo.getAttackBonus(Terrain.HILLS);
        int attackBonusForest = rangedThree.getAttackBonus(Terrain.FOREST);

        assertTrue(attackBonusHills > attackBonusPlains);
        assertTrue(attackBonusPlains > attackBonusForest);
    }
}
