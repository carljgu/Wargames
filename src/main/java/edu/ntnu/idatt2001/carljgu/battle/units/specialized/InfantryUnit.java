package edu.ntnu.idatt2001.carljgu.battle.units.specialized;

import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;

/**
 * Class InfantryUnit that inherits from Unit.
 * InfantryUnit objects implements methods from
 * the superclass.
 *
 * @author Carl Gützkow
 * @version 1.4 01.05.2022
 */
public class InfantryUnit extends Unit {

    /*
    Constants are defined here
    for structure and simplification if they are to be changed.
    Constant attackBonus is the attack bonus for the object's lifetime.
    Constant resistBonus is the resistance bonus for the object's lifetime.
    Constant forest_bonus is a bonus for attacking and defending in a forest.
     */
    private final int ATTACK_BONUS = 2;
    private final int RESIST_BONUS = 1;
    private final int FOREST_BONUS = 3;

    /**
     * Instantiates a new Infantry unit.
     * Uses the constructor of its superclass Unit
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     * @param attack int - damage done, excluding attack bonus, on an enemy unit.
     * @param armor  int - damage resisted, excluding resist bonus, from an enemy unit.
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new Infantry unit with
     * a simplified constructor.
     * Attack is set to 15 and health is set to 10
     * Uses the constructor over since it instantiates other variables
     *
     * @param name   String - a short name of the unit that can't be left with whitespace or empty.
     * @param health int - HP - amount of health unit has. If it hits zero or more, the unit is dead.
     */
    public InfantryUnit(String name, int health) {
        this(name, health, 15, 10);
    }

    /**
     * Overrides the abstract method getAttackBonus from Unit.
     * The attack bonus is increased if the terrain is forest.
     *
     * @param terrain Terrain - the terrain where the attack occurs.
     * @return attackBonus - int - value of the attack bonus
     * used when calculating an enemy unit's health in an attack.
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? ATTACK_BONUS + FOREST_BONUS : ATTACK_BONUS;
    }

    /**
     * Overrides the abstract method getResistBonus from Unit.
     * The resistance bonus is increased if the terrain is forest.
     *
     * @param terrain Terrain - the terrain where the attack occurs.
     * @return resistBonus - int - value of the resist bonus
     * used when calculating this object's health in an attack.
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? RESIST_BONUS + FOREST_BONUS : RESIST_BONUS;
    }
}
