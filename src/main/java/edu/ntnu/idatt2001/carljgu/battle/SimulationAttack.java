package edu.ntnu.idatt2001.carljgu.battle;

import edu.ntnu.idatt2001.carljgu.battle.units.Unit;

/**
 * This class is deprecated.
 * A class for keeping track of which unit
 * has attacked which unit. Helpful when
 * logging out this information.
 *
 * @author Carl Gützkow
 * @version 1.2 09.04.2022
 * @deprecated Provided no extra functionality and made the program harder to read.
 * Additionally, it had high unnecessary coupling with Army and Unit. 06.05.2022
 */
@Deprecated
public class SimulationAttack {

    private final Army attackingArmy;
    private final Army defendingArmy;
    private final Unit attacker;
    private final Unit defender;
    private final Terrain terrain;
    private final int damageDealt;

    /**
     * The constructor for a simulation attack.
     * After checking the units are actually
     * in their corresponding army, the attacker
     * will perform its attack on the defender.
     * The return value of this method is
     * then put into the damage attribute.
     * If the attack gets a defender to less
     * than zero health, it is removed from the army
     *
     * @param attackingArmy Army - the army with the unit attacking the defender
     * @param attacker      Unit - the attacker performing its attack on the defender
     * @param defendingArmy Army - the army with the unit defending from the attacker
     * @param defender      Unit - the unit defending from the attackers attack
     * @param terrain       Terrain - the terrain where the attack occurs.
     * @throws IllegalArgumentException thrown if the attacker is not part of the attacking army or the defender is not part of the defending army.
     */
    public SimulationAttack(Army attackingArmy, Unit attacker, Army defendingArmy, Unit defender, Terrain terrain)
        throws IllegalArgumentException {
        if (!attackingArmy.getAllUnits().contains(attacker) || !defendingArmy.getAllUnits().contains(defender))
            throw new IllegalArgumentException("Unit is not part of the corresponding army");
        this.attackingArmy = attackingArmy;
        this.defendingArmy = defendingArmy;
        this.attacker = attacker;
        this.defender = defender;
        this.terrain = terrain;
        this.damageDealt = attacker.attack(defender, terrain);
        if (defender.getHealth() <= 0) defendingArmy.remove(defender);
    }

    /**
     * Gets the attacking army with the
     * attacking unit.
     *
     * @return attackingArmy - Army - the army with
     * the attacking unit.
     */
    public Army getAttackingArmy() {
        return attackingArmy;
    }

    /**
     * Gets the attacker.
     *
     * @return attacker - Unit - the attacker
     */
    public Unit getAttacker() {
        return attacker;
    }

    /**
     * Gets the defending army with the
     * defending unit.
     *
     * @return defendingArmy - Army - the army with
     * the defending unit.
     */
    public Army getDefendingArmy() {
        return defendingArmy;
    }

    /**
     * Gets the defender.
     *
     * @return defender - Unit - the defender
     */
    public Unit getDefender() {
        return defender;
    }

    /**
     * Gets the damage done by
     * the attacker to the defender.
     *
     * @return damageDone - int - the
     * damage done by the attacker
     * to the defender
     */
    public int getDamageDealt() {
        return damageDealt;
    }

    /**
     * Gets the terrain where
     * the attack occurs.
     *
     * @return terrain - Terrain - the specified terrain.
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * Overrides the toString() method
     * and returns an output used in the
     * GUI. Information displayed is the
     * name of the attacking and defending army,
     * unit information like name and class
     * as well as how much damage was outputted
     * from the attack. In addition, the
     * defender's health after the attack is shown
     *
     * @return String - string representation of an attack
     */
    @Override
    public String toString() {
        int defenderHealth = Math.max(defender.getHealth(), 0);
        return (
            attacker.getClassName() + " " + attacker.getName() + " from " + attackingArmy.getName() + "\n" +
            "did " + damageDealt + " damage to\n" +
            defender.getClassName() + " " + defender.getName() + " from " + defendingArmy.getName() + ".\n" +
            "Current health of the defender is now at " + defenderHealth
        );
    }
}
