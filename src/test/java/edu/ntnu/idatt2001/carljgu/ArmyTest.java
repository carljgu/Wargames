package edu.ntnu.idatt2001.carljgu;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.carljgu.battle.Army;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.RangedUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ArmyTest {

    Army army;

    @Nested
    public class Unit_constructor_throws_exception {

        @Test
        public void name_of_army_can_not_have_whitespace_only() {
            try {
                army = new Army("   ");
                fail();
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Name can not be empty");
            }
        }

        @Test
        public void unit_list_not_defined_creates_new_unit_arraylist() {
            army = new Army("Valid name");

            assertNotNull(army.getAllUnits());
            assertFalse(army.hasUnits());
        }

        @Test
        public void arraylist_with_units_as_parameter_stores_list() {
            ArrayList<Unit> units = new ArrayList<>();
            units.add(new InfantryUnit("valid name", 10));

            army = new Army("Valid name", units);

            assertTrue(army.hasUnits());
        }
    }

    @Nested
    public class Retrival_and_adding_to_unit_list {

        @Test
        public void getting_random_unit_from_army_when_units_is_empty_returns_null() {
            army = new Army("Valid name");

            Unit randomUnit = army.getRandom();

            assertNull(randomUnit);
        }

        @Test
        public void units_with_duplicate_in_army_are_not_added() {
            ArrayList<Unit> units = new ArrayList<>();
            InfantryUnit duplicate = new InfantryUnit("Infantry", 10);
            army = new Army("Army");

            for (int i = 0; i < 10; i++) units.add(duplicate);
            army.addUnits(units);

            assertEquals(1, army.getAllUnits().size());
        }

        @Test
        public void all_units_in_list_are_transferred_to_observable_list() {
            List<Unit> units = new ArrayList<>();
            units.add(new InfantryUnit("Infantry", 10));
            units.add(new RangedUnit("Ranged", 10));

            Army army = new Army("New army", units);

            assertEquals(units, army.getAllUnits());
        }
    }


    @Test
    public void armies_with_the_same_name_and_contains_the_same_units_but_different_arraylist_objects_have_equal_hash_codes() {
        String armyName = "army";
        InfantryUnit unit = new InfantryUnit("infantry", 10);
        ArrayList<Unit> units1 = new ArrayList<>();
        ArrayList<Unit> units2 = new ArrayList<>();

        units1.add(unit);
        units2.add(unit);

        Army armyOne = new Army(armyName, units1);
        Army armyTwo = new Army(armyName, units2);

        assertEquals(armyOne.hashCode(), armyTwo.hashCode());
    }



    @Nested
    public class Army_returns_a_filtered_list {

        @BeforeEach
        public void initializeValidArmyWithSeveralUnitsOfAllClasses() {
            army = new Army("Army");

            ArrayList<Unit> units = new ArrayList<>();

            for (int i = 0; i < 10; i++) {
                units.add(new InfantryUnit("infantry", 10));
                units.add(new RangedUnit("ranged", 10));
                units.add(new CavalryUnit("cavalry", 10));
                units.add(new CommanderUnit("commander", 10));
            }

            army.addUnits(units);
        }

        @Test
        public void only_a_list_of_infantries_are_returned_when_calling_getInfantryUnits() {
            /* This test will be the same for the other classes except cavalry
             * because they have the same logic. Therefore, tests for ranged
             * and commander are not written. */

            List<Unit> infantries = army.getInfantryUnits();

            for (Unit unit : infantries) assertInstanceOf(InfantryUnit.class, unit);

            assertTrue(infantries.size() > 0);
        }

        @Test
        public void only_a_list_of_cavalries_are_returned_when_calling_getCavalryUnits() {
            /* Since getCavalryUnits has a little different logic than the other
             * similar once, it is tested itself. */

            List<Unit> infantries = army.getCavalryUnits();

            for (Unit unit : infantries) {
                assertInstanceOf(CavalryUnit.class, unit);
                assertFalse(unit instanceof CommanderUnit);
            }

            assertTrue(infantries.size() > 0);
        }
    }

    @Nested
    public class Army_strength {

        @Test
        public void army_with_no_units_has_zero_strength() {
            Army army = new Army("Valid name");
            assertEquals(0, army.getCalculatedStrength());
        }

        @Test
        public void army_with_one_basic_infantry_with_ten_health_has_thirty_five_strength() {
            Army army = new Army("Valid name");
            army.addUnit(new InfantryUnit("Infantry with 10 health", 10));
            assertEquals(35, army.getCalculatedStrength());
        }

        @Test
        public void army_with_ten_equal_units_has_strength_divisible_by_ten() {
            Army army = new Army("Valid name");
            int amountOfUnits = 10;
            for (int i = 0; i < amountOfUnits; i++)
                new InfantryUnit("infantry", 1);
            assertEquals(0, army.getCalculatedStrength() % amountOfUnits);
        }

    }
}
