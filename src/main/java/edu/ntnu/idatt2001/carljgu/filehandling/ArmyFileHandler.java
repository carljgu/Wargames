package edu.ntnu.idatt2001.carljgu.filehandling;

import edu.ntnu.idatt2001.carljgu.battle.Army;
import edu.ntnu.idatt2001.carljgu.client.App;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.UnitFactory;
import edu.ntnu.idatt2001.carljgu.battle.units.UnitType;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles writing, reading and importing
 * armies to and from a file.
 *
 * @author Carl Gützkow
 * @version 1.4 1.05.2022
 */
public class ArmyFileHandler {

    private final List<String> readLinesSkipped;
    private final String DELIMITER;

    /**
     * Constructor which initiates the readLinesSkipped arraylist
     * Sets the delimiter to a comma for comma seperated value.
     */
    public ArmyFileHandler() {
        readLinesSkipped = new ArrayList<>();
        DELIMITER = ",";
    }

    /**
     * Gets the amount of units
     * that were skipped when reading from file.
     *
     * @return readLinesSkipped - amount of lines skipped when
     * reading army from file because of invalid unit.
     */
    public List<String> getReadLinesSkipped() {
        return readLinesSkipped;
    }

    /**
     * Creates a file to write on or writes to an existing file.
     * The client should handle any exception that is thrown
     * with an error message the client creates.
     *
     * @param army Army - A collection of units that is written to a file
     * @param filePath String - name of the file and its full path
     * @throws NullPointerException thrown if the army is a null object
     * @throws FileExtensionException thrown if the file extension is not csv
     * because of the file name
     * @throws IOException thrown if an I/O error occurs
     */
    public void writeToFile(Army army, String filePath)
        throws NullPointerException, FileExtensionException, IOException {
        if (army == null) throw new NullPointerException("Army is a a null object");
        if (!filePath.endsWith(".csv")) throw new FileExtensionException("File extension has to be csv");

        try (FileWriter fileWriter = new FileWriter(filePath)) {
            fileWriter.write(army.getName() + "\n");
            for (Unit unit : army.getAllUnits()) {
                fileWriter.write(
                    unit.getClassName() + DELIMITER + unit.getName() + DELIMITER + unit.getHealth() + "\n"
                );
            }
        }
    }

    /**
     * Finds a specified csv file
     * and converts it to an army if it can.
     * If there is a line which has an unreadable unit
     * it will continue on the next line
     * The client should handle any exception that is thrown
     * with an error message the client creates.
     *
     * @param filePath - String - name of the file path to read from
     * @throws IOException thrown if an I/O error occurs
     * @throws FileNotFoundException thrown if the filepath was not found
     * @throws IllegalArgumentException thrown if file extension is not csv or if army could not be created.
     * @return army - Army - an army read from a csv file
     */
    public Army readArmyFromFile(String filePath)
        throws IOException, FileNotFoundException, IllegalArgumentException {
        if (!filePath.endsWith(".csv")) throw new FileExtensionException("File extension has to be csv");

        UnitFactory unitFactory = new UnitFactory();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Army army = new Army(bufferedReader.readLine());
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    String[] unit = line.split(DELIMITER);
                    UnitType unitType = UnitType.getUnitType(unit[0].trim());
                    String name = unit[1].trim();
                    int health = Integer.parseInt(unit[2].trim());

                    army.addUnit(unitFactory.createUnit(unitType, name, health));
                } catch (NumberFormatException e) {
                    readLinesSkipped.add("Line \"" + line + "\" skipped because health is not valid number because \" " + e.getMessage() + " \"");
                } catch (IllegalArgumentException e) {
                    readLinesSkipped.add("Line \"" + line + "\" skipped because \"" + e.getMessage() + "\"");
                } catch (ArrayIndexOutOfBoundsException e) {
                    readLinesSkipped.add("Line \"" + line + "\" skipped because it did not contain enough information");
                }
            }
            return army;
        }
    }

    /**
     * Uses the system default file explorer to get
     * a file path of the user's choice.
     * If a user closes the file chooser,
     * null is returned.
     *
     * @param fileChooserType String - OPEN or SAVE depending on whether
     *                        the user wants to open a file or write to a file
     * @return filePath - String - the file path of a csv file for an army
     */
    public String getFilePath(String fileChooserType) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        File file = null;

        if (fileChooserType.equals("OPEN")) {
            fileChooser.setTitle("Select army from file");
            file = fileChooser.showOpenDialog(App.getStage());
        } else if (fileChooserType.equals("SAVE")) {
            fileChooser.setTitle("Select directory to save army");
            file = fileChooser.showSaveDialog(App.getStage());
        }

        if (file != null)
            return file.getPath();
        return null;
    }
}
