package edu.ntnu.idatt2001.carljgu.battle.units;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * An enum that represents the different unit types
 * that an army can contain.
 *
 * @author Carl Gützkow
 * @version 1.2 16.05.2022
 */
public enum UnitType {

    INFANTRY_UNIT("InfantryUnit"),
    RANGED_UNIT("RangedUnit"),
    CAVALRY_UNIT("CavalryUnit"),
    COMMANDER_UNIT("CommanderUnit");

    private final String className;
    private static final Map<String, UnitType> unitTypes = Arrays.stream(UnitType.values())
            .collect(Collectors.toMap(UnitType::getClassName, Function.identity()));

    /**
     * The enum constructor
     *
     * @param className String - the enum in PascalCase
     */
    UnitType(String className) {
        this.className = className;
    }

    /**
     * Gets the class name from the enum
     *
     * @return className - String - enum in PascalCase
     */
    public String getClassName() {
        return className;
    }

    /**
     * Gets a UnitType enum from a string.
     * If a unit type was not found, null is returned instead.
     *
     * @param readableName String - the enum in PascalCase
     * @return UnitType - the enum respective to the readable name
     */
    public static UnitType getUnitType(String readableName) {
        return unitTypes.getOrDefault(readableName, null);
    }
}
