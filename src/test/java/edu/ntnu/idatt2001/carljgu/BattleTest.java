package edu.ntnu.idatt2001.carljgu;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2001.carljgu.battle.Army;
import edu.ntnu.idatt2001.carljgu.battle.Battle;
import edu.ntnu.idatt2001.carljgu.battle.Terrain;
import edu.ntnu.idatt2001.carljgu.battle.units.Unit;
import edu.ntnu.idatt2001.carljgu.battle.units.specialized.InfantryUnit;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class BattleTest {

    Army armyWithUnit;

    @BeforeEach
    public void initializeValidArmy() {
        InfantryUnit unit = new InfantryUnit("infantry", 10);
        ArrayList<Unit> units = new ArrayList<>();
        units.add(unit);
        armyWithUnit = new Army("army", units);
    }

    @Nested
    public class Battle_constructor_throws_exception {

        @Test
        public void exception_if_any_of_the_armies_are_empty() {
            Army emptyArmy = new Army("empty army");
            try {
                new Battle(armyWithUnit, emptyArmy, Terrain.PLAINS);
                fail();
            } catch (IllegalArgumentException exception) {
                assertEquals("Army can not be empty", exception.getMessage());
            }
        }

        @Test
        public void exception_if_armies_are_equal() {
            try {
                new Battle(armyWithUnit, armyWithUnit, Terrain.PLAINS);
                fail();
            } catch (IllegalArgumentException exception) {
                assertEquals("Armies can not be the same", exception.getMessage());
            }
        }
    }

    @Nested
    public class Battle_simulation {

        @Test
        public void loosing_army_has_no_units_but_winning_army_has() {
            Army secondArmy = new Army("second army");
            secondArmy.addUnit(new InfantryUnit("infantry", 10));

            Battle battle = new Battle(armyWithUnit, secondArmy, Terrain.PLAINS);
            Army winningArmy = battle.simulate();

            assertTrue(winningArmy.hasUnits());
            if (!winningArmy.equals(secondArmy)) assertFalse(secondArmy.hasUnits()); else assertFalse(
                armyWithUnit.hasUnits()
            );
        }

        @Test
        public void exception_thrown_if_simulation_is_run_twice() {
            Army secondArmy = new Army("second army");
            secondArmy.addUnit(new InfantryUnit("infantry", 10));

            Battle battle = new Battle(armyWithUnit, secondArmy, Terrain.PLAINS);
            battle.simulate();

            assertThrows(UnsupportedOperationException.class, battle::simulate);
        }
    }
}
